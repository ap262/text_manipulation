def read_line():
    f= open ('example_text_2.txt','r')
    a=f.readline()
    return a

def read_lines():
    f=open('example_text_2.txt','r')
    b=f.readlines()
    return b

def strip(b):
    c = []
    for line in b:
        c.append(line.rstrip('\n'))
    return c

def count_men(b):
    cnt=0
    for line in b:
        word_list=line.split()
        for word in word_list:
            if word=='men':
                cnt=cnt+1
    return cnt

a= read_line()
b= read_lines()
c= strip(b)
count=count_men(b)

print (count)
